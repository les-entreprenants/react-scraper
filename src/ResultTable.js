import React, { Component } from 'react';
import ReactHtmlParser from 'react-html-parser';




class ResultTable extends Component {
  constructor(props) {
    super(props);
    this.state = {tablerows : ''};

    this.generateResults = this.generateResults.bind(this);
  }

  generateResults = (data) => {
    console.log("Create Table:",data);
    let html =  '<tbody>'; 
    if (data !== undefined) {
      data.forEach( result => {
        html += `<tr>
                    <td>${result.title}</td>
                    <td>${result.developer}</td>
                    <td>${result.appId}</td>
                    <td>
                      <button type="button" class="btn btn-primary btn-pill btn-lg d-table ml-auto mr-auto" data-toggle="modal" data-target="#exampleModal${result.id}">Get Reviews</button>
                    </td>
                  </tr>`;
  
        });
      html += '</tbody>';
      this.setState({ tablerows : html }); 
    }
    }

  componentWillReceiveProps({data}) {
    console.log("Component Update (props):", data);
    this.generateResults(data);
  }


  render() {
    return (
      <div className="ResultTable">
        <table className='table table-hover table-responsive'>
          <thead>
            <tr className='table-header'>
                <th>Title</th>
                <th>Developper</th>
                <th>App ID</th>
                <th>Get Reviews</th>
            </tr>
          </thead>
          { ReactHtmlParser(this.state.tablerows) }
        </table>
    </div>
    );
  }
}
export default ResultTable;




