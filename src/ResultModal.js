import React, { Component } from 'react';
import ReactHtmlParser from 'react-html-parser';


class ResultModal extends Component {
  constructor(props) {
    super(props);
    this.state = {Modals : ''};

    this.generateModals = this.generateModals.bind(this);
  }

  generateModals = (data) => {
    console.log("Create Modals:",data);
    let html; 
    if (data !== undefined) {
      data.forEach( result => {
        console.log(result);
        html += `<div class="modal fade" id="exampleModal${result.id}" tabindex="0" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Get reviews for ${result.title}</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="GET" action="/reviews">
                    <p>Personalize your search</p>
                    <div class="form-group">
                        <label>Sort By</label>
                        <select name="sort">
                            <option defaultValue >Select something</option>
                            <option value="NEWEST">Newest</option>
                            <option value="RATING">Rating</option>
                            <option value="HELPFULNESS">Helpfulness</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Language</label>
                        <select name="lang">
                            <option selected>Select something</option>
                            <option value="en">English</option>
                            <option value="fr">French</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Number of pages (40 results per page)</label>
                        <input class="shadow-sm mb-3 bg-white rounded" name="pages" type="number">
                        <input name="appId" type='hidden' value=${result.appId}>
                    </div>
                    <div>
                        <button type="submit" class="btn btn-primary">Get Reviews</button>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
              <hr class="my-4">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>`;
  
        });
      this.setState({ modals : html }); 
    }
    }

  componentWillReceiveProps({data}) {
    console.log("Component Update (props):", data);
    this.generateModals(data);
  }


  render() {
    return (
      <div className="ResultModals">
          { ReactHtmlParser(this.state.modals) }
    </div>
    );
  }
}
export default ResultModal;




