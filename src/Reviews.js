import React, { Component } from 'react';
import ReactHtmlParser from 'react-html-parser';
import queryString from 'query-string';
import { CSVLink } from "react-csv";


class Reviews extends Component {
  constructor(props) {
    super(props);
    this.state = {results:'', data:[]};

    this.generateResults = this.generateResults.bind(this);
  }

  generateResults = (data) => {
    let html =  '<tbody>'; 
    if (data !== undefined) {
      console.log(data);
      data.forEach( result => {
        html += `<tr>
                    <td>${result.date}</td>
                    <td>${result.score}</td>
                    <td>${result.title}</td>
                    <td>${result.text}</td>
                  </tr>`;
  
        }); 
      html += '</tbody>';
      this.setState({ results : html }); 
      this.setState({data});
    }
    }

    componentDidMount() {
      const values = queryString.parse(this.props.location.search);
      this.setState({app: values.appId});
      console.log(values.appId, values.lang, values.sort, values.pages, `/reviews/${values.appId}`);
      fetch(`/api/reviews/${values.appId}?lang=${values.lang}&sort=${values.sort}&pages=${values.pages}`,
            {
            method: 'POST', 
            headers: {'Accept': 'application/json','Content-Type': 'application/json'},
            body: JSON.stringify({lang: values.lang, sort: values.sort, pages:values.pages})
            }
          )
          .then(response => response.json() )
          .then(data => this.generateResults(data))
          .catch(error => console.log(error));
    }

  render() {
    return (
      <div className="Reviews">
        <CSVLink filename={this.state.app} data={this.state.data} className="btn btn-danger" target="_blank">Download Me</CSVLink>
        <table className='table table-hover table-responsive'>
            <thead>
              <tr className='table-header'>
                  <th>Date</th>
                  <th>Score</th>
                  <th>Title</th>
                  <th>Text</th>
              </tr>
            </thead>
            { ReactHtmlParser(this.state.results) }
          </table>
      </div>
    );
  }
}


export default Reviews;

