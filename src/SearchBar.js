import React, { Component } from 'react';


class SearchBar extends Component {
  constructor(props) {
    super(props);
    this.state = {};

    this.handleChange = this.handleChange.bind(this);
  }


  handleChange(event) {
    let term = event.target.value;
    if (term.length >= 4) {
      this.props.term(term);
    }
  }

  render() {
    return (
      <div className="SearchBar">
        <form>
          <div className="form-group">
            <label htmlFor="term">Search for an app</label>
            <input onChange={this.handleChange} type="text" />
            <p id="subtitle">4 characters min.</p>
          </div>
        </form>
      </div>
    );
  }
}


export default SearchBar;

