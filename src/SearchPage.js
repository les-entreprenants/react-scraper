import React, { Component } from 'react';
import SearchBar from './SearchBar';
import ResultTable from './ResultTable';
import ResultModal from './ResultModal';

class SearchPage extends Component {
  constructor(props) {
    super(props);
    this.state = {term:'', data:[]};
    
    this.getSearch = this.getSearch.bind(this);
    this.getData = this.getData.bind(this);
  }

  getSearch = async (term) => {
    console.log('term:', term)
    await this.setState({term});
    await this.getData();
  }

  getData = async () => {
    console.log('state.term:',this.state.term);
    fetch('/api/search', {method: 'POST', headers: {'Accept': 'application/json','Content-Type': 'application/json'}, body: JSON.stringify({value: this.state.term})})
        .then(response => response.json())
        .then(data => data.Error ? console.log("Debuzz caliss") : this.setState({data}) );
  }


  render() {
    return (
      <div className="main-container">
        <h1>Google Play Scraper</h1>
        <p>Easily search through app reviews and export results in csv for further examination</p>
        <SearchBar term={this.getSearch} />
        <ResultTable data={this.state.data} />
        <ResultModal data={this.state.data} />
      </div>
    );
  }
}

export default SearchPage;

