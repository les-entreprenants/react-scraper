import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import SearchPage from "./SearchPage";
import Reviews from "./Reviews";


export class Home extends Component {
  render() {
    return(
      <div>
        <h1>Welcome Home</h1>
        <ul className="navbar-nav mr-auto">
          <li className="nav-item">
            <Link className='nav-link' to="/search-page/">Search Page</Link>
          </li>
          <li className='nav-item'>
            <Link className='nav-link' to={{ pathname: "/reviews/", search: `appId=com.myfitnesspal.android&sort=NEWEST&lang=en&pages=4` }}>Reviews</Link>
          </li>
        </ul>
      </div>
    )
  }
}

const AppRouter = () => (
  <Router>
    <div>
      <nav className="navbar navbar-dark bg-dark">
       <Link className='nav-link' to="/">Home</Link>>
        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent2" aria-controls="navbarSupportedContent2" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent2">
          <ul className="navbar-nav mr-auto">
            <li className="nav-item">
              <Link className='nav-link' to="/search-page/">Search Page</Link>
            </li>
            <li className='nav-item'>
              <Link className='nav-link' to="/reviews/">Reviews</Link>
            </li>
          </ul>
        </div>
        </nav>
      <Route path="/" exact component={Home} />
      <Route path="/search-page" component={SearchPage} />
      <Route path="/reviews" component={Reviews}  />
    </div>
  </Router>
);

export default AppRouter;